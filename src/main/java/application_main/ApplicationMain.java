package application_main;


import java.util.ArrayList;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class ApplicationMain {

	public static void main(String []args) {
	ConfigurableApplicationContext	context=SpringApplication.run(ApplicationMain.class, args);
		
	Product product=context.getBean(Product.class);
	product.productName="pencil";
	product.price=(double)5;
	product.Quantity=90;
	product.category.name="Regular";
	product.variations.variationList=new ArrayList<String>();
	product.variations.variationList.add("White Color");
	product.printProductDetails();
	}
}