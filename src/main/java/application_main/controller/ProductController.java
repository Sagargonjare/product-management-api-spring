package application_main.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import application_main.Product;

@RestController
public class ProductController {
	@Autowired
	Product product;
	@GetMapping(path="api/product3")
	public Product getProduct() {
		product.productName="Pencil";
		product.price=(double) 15;
		product.Quantity=10110;
		product.category.name="Regular";

		return product;
		
	}
	

}